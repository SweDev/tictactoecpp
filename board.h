/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#ifndef BOARD_H
#define BOARD_H

#include "square.h"

/* the number of squares */
#define N_SQUARES 9

/* -- data strucure for a 3 by 3 tic-tac-toe board --
the board is represented by an array, which
for the case of a 3 by 3 board represents the
squares using the following numbering:
-------
|0|1|2|
-------
|3|4|5|
-------
|6|7|8|
-------
*/

/* Board class */
class Board 
{
public:

	/* Board constructor */
	Board();

	
	/* Board destructor */
	~Board();

	/* display: display the board before starting */
	void display();

	/* board_set_piece_random: places a piece of type piece_type at a
	randomly chosen position, except at the position no_set_index,
	a parameter which is used to prevent the computer from placing
	the piece at the same square as it was taken from */
	void set_piece_random(/* input arguments related to the type of piece stored on the square */
						  int piece_type,
						  /* the square that a piece can not be placed at */
						  int no_set_index) ;

	/* delete_random_circle_piece: delete a circle piece from a randomly chosen square */
	int delete_random_circle_piece(/* input argument that indicate a surtain type of piece on a square */
								   int piece_type) ;

	/* is_blocking_piece: Check if a surtain piece type at a given square is a piece that
	is placed as a blocking piece between two opposing pieces that keeps the opposing piece from creating a win pattern */
	bool is_blocking_piece(/* input argument that indicate a specific square on the board */
						   int square_index, 
						   /* input argument that indicate a surtain type of piece on a square */
						   int piece_type) ;

	/* has_win_pattern: returns true if the board has three pieces
	of type piece_type in a winning configuration, i.e. in a
	horizontal, vertical or diagonal row */
	bool has_win_pattern(/* input arguments related to the type of piece stored on the square */
						 int piece_type);

	/* has_piece: returns true if a piece of type piece_type
	is placed on a specific square, returns false otherwise */
	bool has_piece(/* input arguments related to the type of piece stored on the square */
				   int piece_type,
				   /* input argument that indicate a specific square on the board */
				   int square_index);

	/* is_free: checks if a given square is free */
	bool is_free(/* input argument that indicate a specific square on the board */
				 int square_index);

	/* set_piece: places a piece of type piece_type on a given square */
	void set_piece(/* input arguments related to the type of piece stored on the square */
				   int piece_type, 
				   /* input argument that indicate a specific square on the board */
				   int square_index);

	/* square_remove_piece: removes a piece given by piece_type from a given square */
	void remove_piece(/* input argument that indicate a specific square on the board */
					  int square_index);


private:
	
	/* the squares */
	Square *square[N_SQUARES];

	/* ... prototypes of more private functions follow ... */


} ;


#endif