/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#include "computer_player.h"
#include "board.h"

#include <iostream>

using std::cout;
using std::cin;

/* Computer_Player constructor */
Computer_Player::Computer_Player(int piece_type)
	:Player(piece_type)
{

}

/* Computer_Player destructor */
Computer_Player::~Computer_Player()
{

}

/* make_move: make a move for the computer player */
void Computer_Player::make_move(/* input arguments related to the amount of times the board_play_game runs */
								int n_moves)
{

	/* circle_piece_made_win_move will indicate if the computer made a stratigic move to win the game
	circle_piece_made_blocking_move will indicate if the computer made a stratigic move to block the 
	human from winning */
	bool circle_piece_made_win_move = false, circle_piece_made_blocking_move = false ; 
	
	/* no_set_index is the square index where a piece can not be placed at */
	int no_set_index = 0 ;
  
	/*circle_piece_make_stratigic_move(board,n_moves) ;*/
	
	/* The computer makes a winning move */
	circle_piece_made_win_move = circle_piece_make_win_move(n_moves) ;
	
	/* If the computer has not made a win move then it does a blocking move */
	if(circle_piece_made_win_move == false)
	{
		
		/* The computer did not make a win move so it does a blocking move instead */
		circle_piece_made_blocking_move = circle_piece_make_blocking_move(n_moves) ;

		/* If the computer deos not do a win move or blocking move it makes a random move */
		if(circle_piece_made_blocking_move == false && circle_piece_made_win_move == false)
		{
			
			/* The computer makes a random move */
			circle_piece_make_non_stratigic_move(n_moves) ;
			
		} /* end if-statement */

	} /* end if-statement */

}

/* circle_piece_make_non_stratigic_move: the computer does not follow any stratagy and makes a random move */
void Computer_Player::circle_piece_make_non_stratigic_move(/* input arguments related to the amount of times the board_play_game runs */
														   int n_moves) 
{

	/* A variabel that holds the index/location where a piece will not be placed */
	int no_set_index = 0 ; 
	
	/* check if the computer have made less or exactly 3 moves and if so then do something */
	if(n_moves <= 3)
	{
		
		/* place a CIRCLE_PIECE at a random place at the board that is not the location of no_set_index */
		board->set_piece_random(CIRCLE_PIECE, no_set_index) ;
	
	} /* end if-statement */

	/* check if the computer have made more then 3 moves and if so then do something */
	if(n_moves > 3)
	{
      
		/* remove a CIRCLE_PIECE at a random place at the board */
		no_set_index = board->delete_random_circle_piece(CIRCLE_PIECE) ;
		
		/* place a CIRCLE_PIECE at at a random place at the board */
		board->set_piece_random(CIRCLE_PIECE, no_set_index) ;
		
	} /* end if-statement*/

}

/* circle_piece_make_block_move: the computer makes a move to block the human from winning */
bool Computer_Player::circle_piece_make_blocking_move(/* input arguments related to the amount of times the board_play_game runs */
													  int n_moves) 
{

	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ; 
	
	/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece */
	int circle_piece_index_array[3] = {0} ; 
	
	/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece */
	int cross_piece_index_array[3] = {0} ;
	
	/* Variabels that hold the sum of the magic square values */
	int cross_win_sum = 0, circle_win_sum = 0 ;
		
    /* Variabels that hold the the amount of pieces that exists on the game board */
	int n_circle_piece_type = 0, n_cross_piece_type = 0 ; 
	
	/* A variabel that holds the index/location where a piece will not be placed */
	int no_set_index = 0 ; 
		 	
	/* A variabel used to keep track if a square has recently been placed on the game board */
	bool square_piece_set = false ; 
	
    /* Indexes used for iterating array values */
	int x = 0, y = 0, i ; 

	/* check if the computer have made less or exactly 3 moves and if so then do something */
	if(n_moves <= 3)
	{

		/* For statement checks what kind of piece type that exists on the game board, when a cross piece is found
		it's index/location gets stored in the cross_piece_index_array[i], add some value to the variable cross_win_sum 
		and keep track on how many cross pieces we have on the board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
		
			/* If we have found a cross piece on the game board do something */
			if(board->has_piece(CROSS_PIECE,i) == true)
			{
				
				cross_win_sum += magic_squares[i] ;
				n_cross_piece_type += 1 ;
	    
				cross_piece_index_array[y] = i ;
				y++ ;

			} /* end if-statement */

		} /* end for-statement */

		/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
	
			if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]]))) && ((board->is_free(i)) == true) && (n_cross_piece_type == 3) && (square_piece_set == false))
			{
			
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;

			} /* end if-statement */
			else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]]))) && ((board->is_free(i)) == true ) && (n_cross_piece_type == 3) && (square_piece_set == false) )
			{
			
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;
		
			} /* end else if-statement */
			else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]]))) && ((board->is_free(i)) == true ) && (n_cross_piece_type == 3) && (square_piece_set == false) )
			{
			
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;
		   
			} /* end else if-statement */
			else if( (magic_squares[i] == (15-cross_win_sum)) && (n_cross_piece_type == 2) && ((board->is_free(i)) == true) && (square_piece_set == false) )
			{
			
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;
		
			} /* end else if-statement */

		} /* end for-loop statement */

	} /* end if-statement */ 

	/* check if the computer have made more then 3 moves and if so then do something */
	if(n_moves > 3)
	{

		/* For statement checks what kind of piece type that exists on the game board, when a cross piece is found
		it's index/location gets stored in the cross_piece_index_array[i], keep track on how many cross pieces we 
		have on the board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
			/* If we hava found a cross piece on the game board do something */
			if(board->has_piece(CROSS_PIECE,i) == true)
			{	
				
				cross_piece_index_array[y] = i ;
				n_cross_piece_type += 1 ;
				y++ ;

			} /* end if-statement */

		} /* end for-statement */

		/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
		for(i = 0 ; i < N_SQUARES ; i++)
		{

			if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]]))) && ((board->is_free(i)) == true) && (n_cross_piece_type == 3) && (square_piece_set == false))
			{
			   
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				no_set_index = board->delete_random_circle_piece(CIRCLE_PIECE) ;
				
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;

			} /* end if-statement */
			else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]])) && (board->is_free(i)) == true ) && (n_cross_piece_type == 3) && (square_piece_set == true))
			{
			   
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				no_set_index = board->delete_random_circle_piece(CIRCLE_PIECE) ;
		
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;

			} /* end else if-statement */
			else if( (magic_squares[i] == (15-(magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]])) && (board->is_free(i))  == true ) && (n_cross_piece_type == 3) && (square_piece_set == false))
			{
			   
				/* place a CIRCLE_PIECE in such a way on the board so that it blocks the human 
				from creating a win pattern */
				no_set_index = board->delete_random_circle_piece(CIRCLE_PIECE) ;
		
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;			   
		   
			} /* end else if-statement */

		} /* end for-loop statement */

	} /* end if-statement */

 	
	return square_piece_set ;

}

/* circle_piece_make_win_move: the computer makes a winning move */
bool Computer_Player::circle_piece_make_win_move(/* input arguments related to the amount of times the board_play_game runs */
												 int n_moves)
{

	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ; 
	
	/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece */
	int circle_piece_index_array[3] = {0} ; 
	
	/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece */
	int cross_piece_index_array[3] = {0} ;
	
	/* Variabels that hold the sum of the magic square values */
	int cross_win_sum = 0, circle_win_sum = 0 ;
		
    /* Variabels that hold the the amount of pieces that exists on the game board */
	int n_circle_piece_type = 0, n_cross_piece_type = 0 ; 
	
	/* A variabel that holds the index/location where a piece will not be placed */
	int no_set_index = 0 ; 
		 	
	/* A variabel used to keep track if a square has recently been placed on the game board */
	bool square_piece_set = false ; 
	
    /* Indexes used for iterating array values */
	int x = 0, y = 0, i ; 
	
	/* check if the computer have made less or exactly 3 moves and if so then do something */
	if(n_moves <= 3)
	{

		/* Checks what kind of piece type that exists on the game board, when a circle piece is found
		add some value to the variable circle_win_sum and keep track on how many
		circle pieces we have on the board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
			
			/* If we hava found a circle piece on the game board do something */
			if(board->has_piece(CIRCLE_PIECE,i) == true)
			{  	
				
				circle_win_sum += magic_squares[i] ;
				n_circle_piece_type += 1 ;

			} /* end if-statement */

		} /* end for-statement */	
        

		/* The for-statement checks how if the circle pieces are placad in a specific way that can give a win pattern on the game board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
    
			/*printf("\n21\n") ;*/
			if( (magic_squares[i] == (15-circle_win_sum)) && ((board->is_free(i)) == true) && (square_piece_set == false) && (n_circle_piece_type == 2) )
			{
		       
				/* Place a circle piece on the board to create a win pattern */
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ; 

			} /* end if statement */
	
		}  /* end for-statement */

	} /* end if-statement */


	/* check if the computer have made more then 3 moves and if so then do something */
	if(n_moves > 3)
	{

		/* For statement checks what kind of piece type that exists on the game board, when a circle piece is found
		it's index/location gets stored in the circle_piece_index_array[i] and keep track on how many cross 
		pieces we have on the board */
		for(i = 0 ; i < N_SQUARES ; i++)
		{
			
			/* If we hava found a circle piece on the game board do something */
			if(board->has_piece(CIRCLE_PIECE,i) == true)
			{	
			  	
				circle_piece_index_array[x] = i ;
				n_circle_piece_type += 1 ;
				x++ ;

			} /* end if-statement*/

		} /* end for-statement */
		
		/* The for-statement checks how the pieces are placad at the game board and then uses a stratagy for the computer */
		for(i = 0 ; i < N_SQUARES ; i++)
		{

			if( (magic_squares[i] == (15-(magic_squares[circle_piece_index_array[0]] + magic_squares[circle_piece_index_array[1]]))) && ((board->is_free(i) == true) ) && (n_circle_piece_type == 3) && (square_piece_set == false) )
			{
			   
				/* We remove a CIRCLE_PIECE and place it where it will create a win pattern */
			   
				board->remove_piece(circle_piece_index_array[2]) ;
				   		   
				board->set_piece(CIRCLE_PIECE,i) ;
				square_piece_set = true ;
		   
		   } /* end if-statement */
		   else if( (magic_squares[i] == (15-(magic_squares[circle_piece_index_array[0]] + magic_squares[circle_piece_index_array[2]]))) && ((board->is_free(i)) == true ) && (n_circle_piece_type == 3) && (square_piece_set == false) )
		   {
			   
			   /* We remove a CIRCLE_PIECE and place it where it will create a win pattern */

			   board->remove_piece(circle_piece_index_array[1]) ;
			   
			   board->set_piece(CIRCLE_PIECE,i) ;
		       square_piece_set = true ;
		   
		   } /* end else if-statement */
		   else if( (magic_squares[i] == (15-(magic_squares[circle_piece_index_array[1]] + magic_squares[circle_piece_index_array[2]]))) && ((board->is_free(i)) == true ) && (n_circle_piece_type == 3) && (square_piece_set == false) )
		   {
			   
			   /* We remove a CIRCLE_PIECE and place it where it will create a win pattern */
			   
			   board->remove_piece(circle_piece_index_array[0]) ;
			   
			   board->set_piece(CIRCLE_PIECE,i) ;
		       square_piece_set = true ;

		   } /* end else if-statement */

		} /* end for-statement*/

	} /* end if-statement*/


	return square_piece_set ;

}