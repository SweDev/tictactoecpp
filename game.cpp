/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

#include "game.h"
#include "board.h"
#include "computer_player.h"
#include "human_player.h"


/* Game constructor */
Game::Game()
{

	player[0] = new Computer_Player(CIRCLE_PIECE);
	player[1] = new Human_Player(CROSS_PIECE);
	
	board = new Board();
	
	player[0]->use_board(board);
	player[1]->use_board(board);
	
}

/* Game destructor */
Game::~Game()
{

	delete player[0];
	delete player[1];
	delete board;

}


/* play_game: plays a game */
void Game::play()
{

	/* number of moves */
	int n_moves = 0;

	/* display the board before starting */
	board->display();

	/* flag to finish game loop */
	bool done = false;

	/* game loop, which is exited when there is a winner */
	while (!done)
	{

		n_moves++;

		for (int i = 0; i < N_PLAYERS; i++)
		{

			player[i]->make_move(n_moves);
			cout << "result of move:" << endl;
			
			board->display();
			
			if (player[i]->is_the_winner())
			{
				
				cout << "The player with " << player[i]->get_piece_type()
				<< " won, after " << n_moves << " moves\n";
				done = true;
				break;

			}

		}
	}
}