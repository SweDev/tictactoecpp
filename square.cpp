/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#include <iostream>

using std::cout ;
using std::endl ;

#include "square.h"
#include "board.h"

/* Square constructor */
Square::Square()
{

	free = true;
	piece_type = NO_PIECE;

}

/* is_free: checks if the square is free */
bool Square::is_free()
{
	
	return free ;

}

/* has_piece: returns true if a piece of type piece_type is placed on the square, returns false otherwise */
bool Square::has_piece(/* input arguments related to the type of piece stored on the square */
					   int piece_type)
{
	
	if(this->piece_type == piece_type)
		return true ;
	else
		return false ;

}


/* set_piece: places a piece of type piece_type
on the square */
void Square::set_piece(/* input arguments related to the type of piece stored on the square */
					   int piece_type)
{
	
	this->free = false;
	this->piece_type = piece_type ;

}

/* square_remove_piece: removes a piece given by piece_type from a square */
void Square::remove_piece()
{
  
	/* Here we delete a piece from a square */
	this->free = true ;
	this->piece_type = NO_PIECE ;
	
}

/* set_free: makes the square free- Not implemented */
void set_free()
{


}
