/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#include "player.h"

#include <iostream>

using std::cout;

/* Player constructor */

Player::Player(/* input arguments related to the type of piece stored on the square */
			   int piece_type)
{
	
	this->piece_type = piece_type;
	// set board to NULL, until defined by use_board
	board = NULL;

}

/* Player destructor */
Player::~Player()
{

	board = NULL;

}
/* use_board: makes the player use a board */
void Player::use_board(Board* board)
{

	this->board = board;

}

/* get_piece_type: returns a string with the piece
type used by this player */
string Player::get_piece_type()
{

	if (piece_type == CROSS_PIECE)
	{
		
		return "CROSSES";

	}
	else
	{

		return "CIRCLES";

	}

}

/* is_the_winner: returns true if this player is
the winner of the game */
bool Player::is_the_winner()
{
			
	return board->has_win_pattern(piece_type);

}