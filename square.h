/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#ifndef SQUARE_H
#define SQUARE_H

/* symbols for tic-tac-toe pieces */
#define NO_PIECE -1
#define CROSS_PIECE 0
#define CIRCLE_PIECE 1

/* a square class */
class Square 
{

	public:
	
		/* Square constructor */
		Square();
	
		/* is_free: checks if the square is free */
		bool is_free();

		/* has_piece: returns true if a piece of type piece_type
		is placed on the square, returns false otherwise */
		bool has_piece(/* input arguments related to the type of piece stored on the square */
					   int piece_type);

		/* set_piece: places a piece of type piece_type on the square */
		void set_piece(/* input arguments related to the type of piece stored on the square */
					   int piece_type);

		/* square_remove_piece: removes a piece given by piece_type from a square */
		void remove_piece();

		/* set_free: makes the square free- Not implemented */
		void set_free(); 

	private:

		/* flag to indicate if a square is free or not */
		bool free;
		
		/* the type of piece stored on the square if the
		square is not free, in this case the admissible
		values are CROSS_PIECE and CIRCLE_PIECE,
		otherwise the value NO_PIECE is used */
		int piece_type;

};


#endif

