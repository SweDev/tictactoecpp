/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#include <iostream>

using std::cout;
using std::cin;
using std::endl;

#include "board.h"

/* Board constructor */
Board::Board()
{

	for (int i = 0; i < N_SQUARES; i++)
	{

		square[i] = new Square();
	
	}

}


/* Board destructor */
Board::~Board()
{

	for (int i = 0; i < N_SQUARES; i++)
	{

		delete square[i];

	}

}


/* board_set_piece_random: places a piece of type piece_type at a
randomly chosen position, except at the position no_set_index,
a parameter which is used to prevent the computer from placing
the piece at the same square as it was taken from */
void Board::set_piece_random(int piece_type, int no_set_index)
{

	/* maximum number of iterations */
	const int max_n_iter = 100;

	/* actual number of iterations */
	int n_iter;

	/* flag to indicate that a free place is found */
	int found;

	/* the square index where the piece shall be placed */
	int square_index;

	/* no iterations done */
	n_iter = 0;

	/* the square is not found */
	found = 0;

	/* as long as a square is not found and not too many iterations
	are performed */
	while (!found && n_iter < max_n_iter)
	{
		
		/* one more iteration */
		n_iter++;
		
		/* select a random square, i.e. select a number between 0 and
		N_SQUARES-1 */
		square_index = rand() % N_SQUARES;

		/* check if the index is allowed and the square is free,
		and if this is the case, the piece is placed on the square */
		found = square_index != no_set_index && square[square_index]->is_free();
		if (found)
		{
			
			square[square_index]->set_piece(piece_type);
			
		}
	}
	if (!found)
	{
	
		cout << "ERROR: could not place piece" << endl;

	}
}


/* display: displays a board */
void Board::display()
{
	
	/* loop counter */
	int i ;

	/* array[i] will hold the characters that will be diplayed on the game board instead of numbers */
	char array[9] = {' '} ;
  
	/* We check square[i] with if statments what type of pieces it holds and 
	use the array[i] to store characters at the same squares.These characters will 
	be printed on the game board instead of non user friendly numbers that are stored
	in square[i]. The idea is to print characters and not integers */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
   	
		if(square[i]->has_piece(NO_PIECE) )
			array[i] = ' ' ;

		if(square[i]->has_piece(CROSS_PIECE) )
			array[i] = 'X' ; 

		if(square[i]->has_piece(CIRCLE_PIECE) )
			array[i] = 'O' ;  

	} /* end for-loop */

	/* Now lets display what square[i] holds by the use of array[i] */
	cout << "\n" ;
	cout << "-------\n" ;
	cout << "|" << array[0] << "|" << array[1] << "|" << array[2] << "|" << "\n" ; 
	cout << "-------\n" ;
	cout << "|" << array[3] << "|" << array[4] << "|" << array[5] << "|" << "\n" ;
	cout << "-------\n" ;
	cout << "|" << array[6] << "|" << array[7] << "|" << array[8] << "|" << "\n" ;
	cout << "-------\n" ;

}


/* board_has_win_pattern: check if the human or computer has won */
bool Board::has_win_pattern(/* input argument that indicate a surtain type of piece on a square */
							int piece_type) 
{
	
	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ;
	
	/* win_sum hold a number determined by magic_squares[i] and the position where
	the pieces are placed at the board,
	n_piece_type hold a number that is determined by the amount of pieces placed on 
	the board of a surtain type */
	int win_sum = 0, i, n_piece_type = 0 ;
    
	/* for statement checks if a square holds a specific piece type and if it does 
	the variabel win_sum stores a number that must be 15 for that particular piece 
	type to win the game */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
		
		if(square[i]->has_piece(piece_type) == true)
		{
	    
			win_sum += magic_squares[i] ;
			n_piece_type += 1 ;

		} /* end if statment*/
	
	} /* end for loop */

	/* if statement checks if a particular piece type has won. The piece type need to have the win_sum to hold the value,
	15 and there has to be exactly 3 pieces of a curtain piece type on the board.
	If a surtain piece type has a win pattern of the board this function board_has_win_pattern() returns 1 
	else it returns 0 */
    if( ( win_sum == 15 ) && ( n_piece_type == 3 ) )
      return true ;
	else
	  return false ;

}


/* has_piece: returns true if a piece of type piece_type is placed on the square, returns false otherwise */
bool Board::has_piece(/* input argument that indicate a surtain type of piece on a square */
					  int piece_type, 
					  /* input argument that indicate a specific square on the board */
					  int square_index)
{
	
	return square[square_index]->has_piece(piece_type) ;

}


/* is_free: checks if the square is free */
bool Board::is_free(/* input argument that indicate a specific square on the board */
					int square_index)
{
	
	return square[square_index]->is_free() ;

}


/* set_piece: places a piece of type piece_type on the square */
void Board::set_piece(int piece_type, int square_index)
{
	
	square[square_index]->set_piece(piece_type) ;
	
}


/* square_remove_piece: removes a piece given by piece_type from a given square */
void Board::remove_piece(/* input argument that indicate a specific square on the board */
						 int square_index)
{
  
	/* Here we delete a piece from a square */
	square[square_index]->remove_piece() ;
	
}


/* is_blocking_piece: Check if a surtain piece type at a given square is a piece that
is placed as a blocking piece between two opposing pieces that 	
keeps the opposing piece from creating a win pattern */
bool Board::is_blocking_piece(/* input argument that indicate a specific square on the board */
							  int square_index, 
							  /* input argument that indicate a surtain type of piece on a square */
							  int piece_type)
{

	/* magic_squares[i]: holds a special sequence of int values that will give the sum of 15 
	when 3 the same pieces are placed horozontally, vertically or diogonally */
	int magic_squares[9] = {4,3,8,9,5,1,2,7,6} ; 
	
	/* circle_piece_index_array[i] is an array that holds the exact index/location of a circle piece on the board */
	int circle_piece_index_array[3] = {0} ; 
	
	/* cross_piece_index_array[i] is an array that holds the exact index/location of a cross piece on the board */
	int cross_piece_index_array[3] = {0} ;
	
	/* cross_win_sum and circle_win_sum hold a number determined by magic_squares[i] and the position where
	the pieces are placed at the board */
	int cross_win_sum = 0, circle_win_sum = 0 ;
		
    /* n_circle_piece_type and n_cross_piece_type both hold a number that is determined by the amount of pieces 
	placed on the board of a surtain type */
	int n_circle_piece_type = 0, n_cross_piece_type = 0 ;

	/* Indexes used for iterating array values */
	int x = 0, y = 0, i ; 

	/* for statement checks if a square holds a specific piece type and if it does 
	we store the exact location of that piece */
	for(i = 0 ; i < N_SQUARES ; i++)
	{
		
		if(square[i]->has_piece(piece_type) == true)
		{	
			
			circle_piece_index_array[x] = i ;
			x++ ;
			
		} /* end if statement*/
		
		if(square[i]->has_piece(piece_type) == true)
		{
	    
			cross_piece_index_array[y] = i ;
			y++ ;

		} /* end if statement*/

	} /* end for statement*/

	/* if statements checks if a square at the given location, square_index, is placed as a blocking piece and if it is
	   the board_is_blocking_piece() function returns 1 */
	if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]] + magic_squares[square_index] ) == 15 ) )
	{
		
		return true ;
		
	} /* end if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]] + magic_squares[square_index] ) == 15 ) )
	{

		return true ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[1]] + magic_squares[square_index] ) == 15 ) )
	{
		

		return true ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{

		return true ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{

		return true ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[0]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{

		return true ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{

		return true ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{

		return true ;
		   
	} /* end else if statement */
	else if( ( ( magic_squares[cross_piece_index_array[1]] + magic_squares[cross_piece_index_array[2]] + magic_squares[square_index] ) == 15 ) )
	{

		return true ;
		   
	} /* end else if statement */
	else
	{

		return false ;

	} /* end else statement */

}

/* board_delete_random_circle_piece: delete a circle piece from a randomly chosen square */
int Board::delete_random_circle_piece(/* input argument that indicate a surtain type of piece on a square */
									  int piece_type) 
{

  /* maximum number of iterations */
  const int max_n_iter = 100 ;
  
  /* actual number of iterations */
  int n_iter ;

  /* flag to indicate that a free place is found */
  int found ;
  
  /* the square index where the piece shall be removed from */
  int square_index ;
  
  /* no iterations done */
  n_iter = 0 ;
  
  /* the square is not found */
  found = 0 ;
  
  /* as long as a square is not found and not too many iterations
  are performed */
  while (!found && n_iter < max_n_iter)
  {
    
	/* one more iteration */
    n_iter++ ;
    
	/* select a random square, i.e. select a number between 0 and
    N_SQUARES-1 */
    square_index = rand() % N_SQUARES ;
    
	/* check if the index is allowed and the square is a CIRCLE_PIECE,
    and if this is the case, the piece is moved from the square */
    found = (square[square_index]->has_piece(piece_type) == true) && ((is_blocking_piece(square_index,piece_type)) == false) ;
				
	//(square_index != no_set_index) && (square_is_free(&board->square[square_index]))
    
	if (found)
	{
      
      /* Here we delete the CIRCLE_PIECE at the given position a, 
      given by the human player */
	 
	  square[square_index]->remove_piece() ;
	  
	} /* end if-statement */
  
  } /* end while-statement */
  
  if (!found)
  {
  
    printf("ERROR: could not move piece\n");
  
  } /* end if-statement */


  return square_index ;

}