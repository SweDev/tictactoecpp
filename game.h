/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#ifndef GAME_H
#define GAME_H

#include "player.h"
#include "board.h"

#define N_PLAYERS 2

/* Game class */
class Game
{
	
	public:
	
	/* Game constructor */
	Game();

	/* Game destructor */
	~Game();

	/* play: plays a game */
	void play();
	
	private:
	
	/* the players */
	Player *player[N_PLAYERS];
	
	/* the board */
	Board *board;

};

#endif