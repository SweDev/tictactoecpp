/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/

#ifndef COMPUTER_PLAYER_H
#define COMPUTER_PLAYER_H

#include "board.h"
#include "square.h"

#include "player.h"

/* Computer_Player class */
class Computer_Player : public Player
{

	public:
	
		/* Computer_Player constructor */
		Computer_Player(/* input arguments related to the type of piece stored on the square */
						int piece_type);

		/* Computer_Player destructor */
		~Computer_Player();

		/* make_move: makes a move */
		void make_move(/* input arguments related to the amount of times the board_play_game runs */
					   int n_moves);
		
	private:

		/* nothing to add at the moment */

		/* circle_piece_make_non_stratigic_move: the computer does not follow any stratagy and makes a random move */
		void circle_piece_make_non_stratigic_move(/* input arguments related to the amount of times the board_play_game runs */
												  int n_moves) ;

		/* circle_piece_make_block_move: the computer makes a move to block the human from winning */
		bool circle_piece_make_blocking_move(/* input arguments related to the amount of times the board_play_game runs */
											 int n_moves) ;

		/* circle_piece_make_win_move: the computer makes a winning move */
		bool circle_piece_make_win_move(/* input arguments related to the amount of times the board_play_game runs */
										int n_moves) ;
		
};


#endif