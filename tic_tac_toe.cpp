/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/

#include <iostream>

/* use stdlib.h to get access to random
number generation functionality */
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

#include "game.h"


/* egna funktionsprototyper */

/* initialise random number handling */
void init_random_number_generation(void) ;


/* main function */
int main()
{

	/* the game */
	Game *game = new Game();

	/* print welcoming text */
	cout << "Tic-tac-toe game" << endl;

	/* initialise random number handling */
	init_random_number_generation();

	/* play the game */
	game->play();

	/* clean up */
	delete game;
	
	/* all is well */
	return 0;

}


/* egna funktioner  */

/* initialise random number handling */
void init_random_number_generation(void)
{

	/* initialise random number generator, using a specified seed,
    resulting in the same number sequence (when calling rand())
    for all executions of the program
    srand(12345); */
    
	/* initialise random number generator, using a time dependent seed,
    resulting in different number sequences (when calling rand())
    for different executions of the program */
    srand(time(NULL));

}
