/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#ifndef PLAYER_H
#define PLAYER_H

#include "board.h"
#include "square.h"

#include <string>
using std::string;

/* Player class */
class Player
{

	public:

		/* Player constructor */
		Player(/* input arguments related to the type of piece stored on the square */
			   int piece_type);

		/* Player destructor */
		virtual ~Player();

		/* use_board: makes the player use a board */
		void use_board(Board *board);

		/* get_piece_type: returns a string with the piece
		type used by this player */
		string get_piece_type();

		/* make_move: makes a move */
		virtual void make_move(/* input arguments related to the amount of times the board_play_game runs */
							   int n_moves) = 0;

		/* is_the_winner: returns true if this player is
		the winner of the game */
		bool is_the_winner();

    protected:

		/* the piece_type for this player */
		int piece_type;

		/* the board */
		Board *board;
};


#endif