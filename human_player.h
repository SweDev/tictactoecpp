/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#ifndef HUMAN_PLAYER_H
#define HUMAN_PLAYER_H


#include "board.h"
#include "square.h"

#include "player.h"

/* Human_Player class */
class Human_Player : public Player
{
public:

	/* Human_Player constructor */
	Human_Player(/* input arguments related to the type of piece stored on the square */
				 int piece_type);
	
	/* Human_Player destructor */
	~Human_Player();
	
	/* make_move: makes a move */
	void make_move(/* input arguments related to the amount of times the board_play_game runs */
				   int n_moves);
	
private:

	/* nothing to add at the moment */

};


#endif