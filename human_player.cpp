/******************************/ 
/*							  */
/*	Namn:	  Patrik Paradis  */
/*	Dator ID: TDE02018        */
/*	Datum: 	  2007-08-29	  */
/*							  */
/******************************/


#include "human_player.h"

#include <iostream>

using std::cout;
using std::cin;

/* Human_Player constructor */
Human_Player::Human_Player(/* input arguments related to the type of piece stored on the square */
						   int piece_type)
	:Player(piece_type)
{

}

/* Human_Player destructor */
Human_Player::~Human_Player()
{

}

/* make_move: make a move for the human player */
void Human_Player::make_move(/* input arguments related to the amount of times the board_play_game runs */
							 int n_moves)
{

	/* variabels used to store input from the user */
	int i = 0, a = 0, b = 0 ;
   
	/* Check how many moves has been made so far in the game run */
	if(n_moves>3)
	  {

		do
		{
          
	      /* Ask the user "from where to where? two numbers in the range [0-8]: " 
	      and store the input into the variables a and b */
		  printf("from where to where? two numbers in the range [0-8]: ") ;
	      cin >> a >> b ;
			
		} while (!((board->is_free(b)) == true)) ; /* end while statement */
		
		/* Here we delete the CROSS_PIECE at the given position a, 
        given by the human player */
		
		board->remove_piece(a) ;
		
		/* Here we actually place the CROSS_PIECE at the given position i, 
        given by the human player */
		board->set_piece(CROSS_PIECE,b) ;

	  } /* end if statement */
	  	  
   if(n_moves <= 3)
   {
	
     do
	 {
      
	    /* Ask the user "to where? a number in the range [0-8]: " 
	    and store the input into the variable i */
        printf("to where? a number in the range [0-8]: ") ;
        cin >> i ;
	  
	 } while(!(board->is_free(i) == true)) ; /* end while statment */

	   /* Here we actually place the CROSS_PIECE at the given position i, 
       given by the human player */
       board->set_piece(CROSS_PIECE,i) ;
		
   }


}
